$(document).ready(function () {
    $('.best-offer-s').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3
                }
            }
        ]
    });

    $('.populars-s').slick({
        slidesToScroll: 1,
        variableWidth: true,
        prevArrow: '<div class="slider-arrow slider-arrow__prev"><i class="icon-angle-left"></i></div>',
        nextArrow: '<div class="slider-arrow slider-arrow__next"><i class="icon-angle-right"></i></div>'
    });

    $('.comments-slider').slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        prevArrow: '<div class="slider-arrow slider-arrow__prev"><i class="icon-angle-left"></i></div>',
        nextArrow: '<div class="slider-arrow slider-arrow__next"><i class="icon-angle-right"></i></div>',
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 1,
                    arrows: false,
                    dots: true
                }
            }
        ]
    })
        .on('setPosition', function (event, slick) {
            slick.$slides.css('height', slick.$slideTrack.height() + 'px');
        });

    $('.partners-slider').slick({
        variableWidth: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        prevArrow: '<div class="slider-arrow slider-arrow__prev"><i class="icon-angle-left"></i></div>',
        nextArrow: '<div class="slider-arrow slider-arrow__next"><i class="icon-angle-right"></i></div>',
        responsive: [


            {
                breakpoint: 992,
                settings: {
                    arrows: false,
                    dots: true
                }
            }
        ]
    });

    $('.partners-slider-n').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        prevArrow: '<div class="slider-arrow slider-arrow__prev"><i class="icon-angle-left"></i></div>',
        nextArrow: '<div class="slider-arrow slider-arrow__next"><i class="icon-angle-right"></i></div>',
        responsive: [
            {
                breakpoint: 1280,
                settings: {
                    slidesToShow: 5
                }
            },
            {
                breakpoint: 992,
                settings: {
                    arrows: false,
                    dots: true,
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    dots: true,
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 568,
                settings: {
                    arrows: false,
                    dots: true,
                    slidesToShow: 2
                }
            }
        ]
    });

    $('.expert-opinions-s').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: false,
        dots: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    dots: true
                }
            }
        ]
    });

    $('.select-styler').select2();
    $('.select-styler-img').select2({
        templateResult: formatState,
        templateSelection: formatState
    });

    function formatState (opt) {
        if (!opt.id) {
            return opt.text.toUpperCase();
        }

        var optimage = $(opt.element).attr('data-image');
        console.log(optimage)
        if(!optimage){
            return opt.text.toUpperCase();
        } else {
            var $opt = $(
                '<span><img class="select-styler-img__img" src="' + optimage + '" /> ' + opt.text.toUpperCase() + '</span>'
            );
            return $opt;
        }
    };
    //
    $('.comment-rating__list-item').click(function () {
        var activeClass = 'comment-rating__list-item--active';
        $(this).addClass(activeClass).nextAll('.comment-rating__list-item').removeClass(activeClass);
        $(this).prevAll('.comment-rating__list-item').addClass(activeClass);
    });

    //  range slider
    $( "#filter-amount" ).slider({
        range: "min",
        value: 0,
        min: 1,
        max: 50000,
        slide: function( event, ui ) {
            $( "#filter-amount" ).siblings('.filter__input').val( ui.value + " грн" );
        }
    });
    $( "#filter-amount" ).siblings('.filter__input').keyup(function(){
        var num = $(this).val();

        $( "#filter-amount" ).slider("value", num);
    });

    $( "#filter-time" ).slider({
        range: "min",
        value: 0,
        min: 1,
        max: 90,
        slide: function( event, ui ) {
            $( "#filter-time" ).siblings('.filter__input').val( ui.value);
        }
    });
    $( "#filter-time" ).siblings('.filter__input').keyup(function(){
        var num = $(this).val();

        $( "#filter-time" ).slider("value", num);
    });
    //
    $( "#filter-time-2" ).slider({
        range: "min",
        value: 0,
        min: 1,
        max: 90,
        slide: function( event, ui ) {
            $( "#filter-time-2" ).siblings('.filter__input').val( ui.value);
        }
    });
    $( "#filter-time-2" ).siblings('.filter__input').keyup(function(){
        var num = $(this).val();

        $( "#filter-time-2" ).slider("value", num);
    });


    // .subscription

    if($('.subscription').length) {
        var cont = $('.subscription'),
            wHeight = $('.container-a__aside').height(),
            activeClass = 'subscription--fixed',
            position = cont.offset();

        function subscription() {
            var scrollTop = $(window).scrollTop();

            if( scrollTop >= position.top - 100 && scrollTop < 57 + wHeight) {
                cont.addClass(activeClass);
                cont.css({
                    top: 15,
                    left: position.left
                });
            }else {
                cont.removeClass(activeClass);
                cont.css({
                    top: 'auto',
                    left: 'auto'
                });
            }
        }

        $(window).scroll(function () {
            subscription();
        });

        $(window).resize(function () {
            subscription();
        });
    }

    // search
    $('.header__search-btn').click(function () {
        $("#overlay").fadeToggle();
        $('.search-cont').toggleClass('search-cont--active');
    });

    $('.search-cont__close,#overlay').click(function (e) {
        e.preventDefault();

        $("#overlay").fadeOut();
        $('.search-cont').removeClass('search-cont--active');
    });

    // menu
    if(window.innerWidth > 1024) {
        $('.header-menu__link').mouseenter(function () {
            var menu = $(this).data('drop');

            $('.menu-drop').removeClass('menu-drop--active');
            $('.header-menu__link').removeClass('header-menu__link--active');
            $(this).addClass('header-menu__link--active');
            $(menu).addClass('menu-drop--active');
        });

        $(document).on('mouseleave','.header__cont', function () {
            $('.menu-drop').removeClass('menu-drop--active');
            $('.header-menu__link').removeClass('header-menu__link--active');
        });
    }else {
        // .btn-menu
        $('.btn-menu').click(function (e) {
            e.preventDefault();

            $(this).toggleClass('btn-menu--active');
            $('.header__cont').toggleClass('header__cont--active');
            $('body').addClass('no-scroll');
        });

        $('.header__menu-close').click(function(e){
            e.preventDefault();
            $('.btn-menu').removeClass('btn-menu--active');
            $('.header__cont').removeClass('header__cont--active');
            $('body').removeClass('no-scroll');
        });

        $('.header-menu__link').click(function(e){
            e.preventDefault();
            var menu = $(this).data('drop');

            $('.menu-drop').removeClass('menu-drop--active');
            $('.header-menu__link').removeClass('header-menu__link--active');
            $(this).addClass('header-menu__link--active');
            $(menu).addClass('menu-drop--active');
        });

        $('.menu-drop__back').click(function (e) {
            e.preventDefault();
            $('.menu-drop').removeClass('menu-drop--active');
            $('.header-menu__link').removeClass('header-menu__link--active');
        });
    }

    // footer
    if(window.innerWidth <= 568) {
        $('.footer__item-header').click(function () {
            $(this).toggleClass('footer__item-header--active').siblings('.footer__list').slideToggle();
        });
    }

    // .loans-top-slider

    if(window.innerWidth <= 1024) {
        $('.loans-slider').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: true,
            arrows: false
        });
    }

    $('.cards-best__list').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        dots: false,
        arrows: false,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    dots: true
                }
            }
        ]
    });
    //
    $('.news-lg').slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        arrows: false,
        responsive: [
            {
                breakpoint: 660,
                settings: {
                    slidesToShow: 1,
                    dots: true
                }
            }
        ]
    });
    // site-map__item-header
    $('.site-map__item-header').click(function () {
        var activeClass = 'site-map__item-header--active',
            drop = '.site-map__item-cont';
        $(this).toggleClass(activeClass).siblings(drop).slideToggle();
        $('.site-map__item-header').not($(this)).removeClass(activeClass).siblings(drop).slideUp();
    });
    //
    $( ".loan-calculator__date-input" ).datepicker();
});

